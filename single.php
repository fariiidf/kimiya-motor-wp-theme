<?php
get_header();

if(get_the_ID() != null){
    if(get_field("visit_count", get_the_ID()) != NULL)
        update_field("visit_count", intval(get_field("visit_count", get_the_ID()))+1, get_the_ID());
    else
        update_field("visit_count", "20", get_the_ID());
}
?>

<div class="container singleBlogTop">
    <div class="row">
        <div class="col-12 text-center">
            <h3 class="section-title fs-3"><?=the_title()?></h3>
            <div class="mt-3 d-flex justify-content-center align-items-center">
                <span><i
                        class="bi bi-eye fs-3 text-muted d-inline-block border-end border-2 border-secondary lh-sm pe-3 me-3"></i></span>
                <span class="fw-bold text-black-50 "><?=the_field("visit_count")?></span>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mt-3 singleBlogContent">
        <div class="col-md-12 text-start text-md-center">
            <p class="text-muted my-3 ">
                <?=the_content()?>
            </p>
        </div>
    </div>

</div>

<?php
get_footer();
?>