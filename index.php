<?php include("header.php"); ?>


<!-- learn more -->
<div class="container-fluid">
  <div class="row">
    <div class="col-12 indexTopContainer">
      <div class="indexTopText">
        <h1 class="mb-0 fw-bold text-primary fs-2"><?=get_field("about_title", 5)?></h1>
        <p class="lead fw-bold"><?=get_field("about_subtitle", 5)?></p>
        <span class="span"></span>
        <p class="text-muted">
          <?=get_field("about_text", 5)?>
        </p>
        <a href="<?=site_url("/about-us/")?>" class="primary-button mb-3 d-inline-block wow fadeInDown">بیشتر بدانید</a>
      </div>
      <img src="<?=get_field("about_image", 5)?>" class="indexTopImg" />
    </div>
  </div>
</div>
<!-- products -->
<div class=" container">
  <div class="row">
    <div class="col-12 text-center">
      <h3 class="section-title">محصــولات</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-md-10 offset-md-1 text-center">
      <ul class="nav justify-content-around justify-content-md-center nav-pills mt-3" id="pills-tab" role="tablist">
        <?php
            $term_args = array(
              'post_type'              => 'products',
              'taxonomy'               => 'products_categories',
              'hide_empty'             => false,
              'fields'                 => 'all'
            );
            $cats = new WP_Term_Query( $term_args );
            foreach($cats->get_terms() as $index=>$cat) {
              if($cat->parent != 0){
          ?>

        <li class="nav-item " role="presentation">
          <button class="nav-link text-muted <?=($index==0)?'active' : ''?>" id="pills-home-tab" data-bs-toggle="pill"
            data-bs-target="#pills-<?=$index?>" type="button" role="tab" aria-controls="pills-home"
            aria-selected="true">
            <?php echo $cat->name; ?>
          </button>
        </li>
        <?php } //echo get_category_link( $cat->term_id ) ?>
        </a>
        <?php
            }
          ?>
      </ul>
      <div class="tab-content" id="pills-tabContent">
        <?php
            foreach($cats->get_terms() as $index=>$cat) {
            ?>
        <div class="tab-pane fade show <?=($index==0)?'active' : ''?> " id="pills-<?=$index?>" role="tabpanel">
          <div class="indexProductsSplide">
            <div class="splide__arrows">
              <button class="splide__arrow splide__arrow--prev bg-transparent">
                <span class="right-arrow" aria-hidden="true"></span>
              </button>
              <button class="splide__arrow splide__arrow--next bg-transparent">
                <span class="left-arrow" aria-hidden="true"></span>
              </button>
            </div>
            <div class="splide__track">
              <ul class="splide__list">
                <?php
                  $args = array(
                      'post_type'      => 'products',
                      'posts_per_page' => 6,
                      'tax_query' => array(
                          array (
                              'taxonomy' => 'products_categories',
                              'field' => 'term_id',
                              'terms' => $cat->term_id,
                          )
                      )
                  );
                  $loop = new WP_Query($args);
                  $animDelay = 200;
                  while ( $loop->have_posts() ) {
                      $loop->the_post();
                      $animDelay += 300;
                      ?>
                <li class="splide__slide d-flex justify-content-center padding wow fadeIn"
                  data-wow-delay="<?=$animDelay?>ms">
                  <div class="card card-product border-0 shadow" style="width: 18rem">
                    <div class="d-flex justify-content-center">
                      <a href="<?=get_the_permalink()?>"><img src="<?=get_the_post_thumbnail_url();?>"
                          class="card-img-top" alt="..." /></a>
                    </div>
                    <div class="d-flex justify-content-center">
                      <div class="card-seprator text-center"></div>
                    </div>
                    <div class="card-body itemBox">
                      <a href="<?=get_the_permalink()?>">
                        <h5 class="card-title text-start">
                          <?= get_the_title(); ?>
                        </h5>
                      </a>
                      <p class="card-text text-muted text-start">
                        <?= get_the_excerpt(); ?>
                      </p>
                      <div class="d-flex justify-content-center">
                        <a href="<?=get_the_permalink()?>" class="primary-button text-center">مشاهده محصول<i
                            class="bi bi-chevron-<?=get_locale()=="fa_IR" ? "left" : "right"?>"></i></a>
                      </div>
                    </div>
                  </div>
                </li>
                <div class="entry-content">
                </div>
                <?php
                  }
                  ?>

              </ul>
            </div>
          </div>
        </div>
        <?php
            }?>
      </div>
    </div>
  </div>
</div>
<!-- video -->
<div class="container videoMainContainer">
  <div class="row g-0">
    <div class="col-lg-10 p-md-4 video-container offset-lg-1">
      <video controls poster="<?=get_field("video_cover", 5)?>">
        <source src="<?=get_field("video", 5)?>" type="video/mp4" />
      </video>
      <div class="text-video">
        <div class="text-video-container">
          <h2 class="text-white text-video-title mb-0"><?=get_field("video_title", 5)?></h2>
          <h3 class="text-white-50 text-video-subtitle h4 mb-0"><?=get_field("video_subtitle", 5)?></h3>
          <p class="text-white-50">
            <?=get_field("video_text", 5)?>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- colaborations with -->
<div class="container">
  <div class="row">
    <div class="col-12 text-center mt-3">
      <h3 class="section-title "><?=get_field("collab_title", 5)?></h3>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-8 text-start text-md-center">
      <p class="text-muted my-3 font18 text-justify">
        <?=get_field("cooperates_text", 5)?>
      </p>
    </div>
  </div>
  <div class="row my-4 justify-content-center align-items-center">
    <div class="col-lg-10">
      <div class="row logosContainer">
        <div class="col-6 col-md-3 d-flex justify-content-center">
          <img src="<?=get_field("logo_1", 5)?>" class="cars-logo img-fluid wow zoomIn" data-wow-delay="500"
            alt="kia-logo" />
        </div>
        <div class="col-6 col-md-3 d-flex justify-content-center">
          <img src="<?=get_field("logo_2", 5)?>" class="cars-logo img-fluid wow zoomIn" data-wow-delay="700"
            alt="Peugeot-logo" />
        </div>
        <div class="col-6 col-md-3 d-flex justify-content-center">
          <img src="<?=get_field("logo_3", 5)?>" class="cars-logo img-fluid wow zoomIn" data-wow-delay="1000"
            alt="saipa-logo" />
        </div>
        <div class="col-6 col-md-3 d-flex justify-content-center">
          <img src="<?=get_field("logo_4", 5)?>" class="cars-logo img-fluid wow zoomIn" data-wow-delay="1300"
            alt="iranKhodro-logo" />
        </div>
      </div>
    </div>
  </div>
</div>
<!-- colleagues -->
<div class="container">
  <div class="row mt-2">
    <div class="col-12 text-center">
      <h3 class="section-title"><?=get_field("personel_title", 5)?></h3>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-8 text-md-center">
      <p class="text-muted mt-3 mb-3 font18 text-justify">
        <?=get_field("personel_text", 5)?>
      </p>
    </div>
  </div>
  <div id="splideProducts" class="splide ">
    <div class="splide__arrows">
      <button class="splide__arrow splide__arrow--prev bg-transparent">
        <span class="right-arrow" aria-hidden="true"></span>
      </button>
      <button class="splide__arrow splide__arrow--next bg-transparent">
        <span class="left-arrow" aria-hidden="true"></span>
      </button>
    </div>
    <div class="splide__track">
      <ul class="splide__list">

        <?php
              $args = array(
                  'post_type'      => 'personnel',
                  'posts_per_page' => 10,
              );
              $loop = new WP_Query($args);
              $animDelay = 200;
              while ( $loop->have_posts() ) {
                  $loop->the_post();
                  $animDelay += 300;
                  ?>
        <li class="splide__slide d-flex justify-content-center padding personelItem wow fadeInUp"
          data-wow-delay="<?=$animDelay?>ms">
          <div class="card card-partner border-0 shadow" style="width: 18rem">
            <div class="d-flex justify-content-center   ">
              <img src="<?=get_the_post_thumbnail_url()?>" class="card-img-top w-100 h-100 " alt="..." />
            </div>
            <div class="card-body shadow-lg itemBox">
              <div class="card-body-badge"></div>
              <h5 class="card-title text-center h6 fw-bold"><?= get_the_title(); ?></h5>
              <p class="card-text text-muted text-center">
                <?= get_the_excerpt(); ?>
              </p>
            </div>
          </div>
        </li>
        <?php } ?>
      </ul>
    </div>
  </div>
</div>
<!-- form -->
<div class="container my-1 indexContactContainer">
  <div class="row bg-form ltr">
    <div class="col-12 bg-crop p-5">
      <div>
        <h4 class="text-white border-bottom border-2 border-danger d-inline-block pb-2">
          <?=get_field("contact_title", 5)?></h4>
        <p class="text-white-50 subtitle"><?=get_field("contact_subtitle", 5)?></p>
      </div>
      <div class="row">
        <div class="col-md-4 ">
          <?=do_shortcode('[contact-form-7 id="211" title="فرم ارسال پیام فوری"]');?>
        </div>
      </div>

    </div>
  </div>
</div>
<!--new blogs -->
<div class="container mb-5">
  <div class="row">
    <div class="col-12 text-center">
      <h3 class="section-title fs-2"><?=get_field("blog_title", 5)?></h3>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="splide">
        <div class="splide__arrows">
          <button class="splide__arrow splide__arrow--prev bg-transparent">
            <span class="right-arrow" aria-hidden="true"></span>
          </button>
          <button class="splide__arrow splide__arrow--next bg-transparent">
            <span class="left-arrow" aria-hidden="true"></span>
          </button>
        </div>
        <div class="splide__track">
          <ul class="splide__list">

            <?php
              $args = array(
                  'post_type'      => 'post',
                  'posts_per_page' => 10,
              );
              $loop = new WP_Query($args);
              $animDelay = 200;
              while ( $loop->have_posts() ) {
                  $loop->the_post();
                  $animDelay += 300;
                  ?>

            <li class="splide__slide d-flex justify-content-center padding wow fadeIn"
              data-wow-delay="<?=$animDelay?>ms">
              <div class="card position-relative  border-0 shadow" style="width: 18rem">
                <div class="d-flex justify-content-center">
                  <a href="<?=get_the_permalink()?>" target="_blank" class="w-100"><img
                      src="<?=get_template_directory_uri()?>/assets/images/blog.jpg" class="card-img-top w-100 h-100"
                      alt="..." /></a>
                </div>
                <div class="d-flex justify-content-start ltr">
                  <div class="badge-blog ltr">
                    <span class="card-blog-date"><?=get_the_date()?></span>
                    <p class="text-detail h6 text-muted ltr"><?=get_the_category()[0]->name?></p>
                  </div>
                </div>
                <div class="card-body itemBox">
                  <a href="<?=get_the_permalink()?>" target="_blank">
                    <h5 class="card-title text-start text-primary"><?=get_the_title()?></h5>
                  </a>
                  <p class="card-text text-muted text-start small">
                    <?=get_the_excerpt()?>
                  </p>
                  <div class="d-flex justify-content-center">
                    <a target="_blank" href="<?=get_the_permalink()?>" class="primary-button text-center">مطالعه مطلب<i
                        class="bi bi-chevron-<?=get_locale()=="fa_IR" ? "left" : "right"?>"></i></a>
                  </div>
                </div>
              </div>
            </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<?php include("footer.php"); ?>