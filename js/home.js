window.addEventListener("scroll", function () {
  var header = document.querySelector("#navTest");
  var logo = document.querySelector("#navTest .test");
  header.classList.toggle("sticky-test", window.scrollY > 550);
  //logo.classList.toggle("hiden-logo", window.scrollY > 550);
});

$(function(){
  new WOW().init(); 
});

var map = L.map("map").setView([51.505, -0.09], 12);
var tiles = L.tileLayer(
  "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
  {
    maxZoom: 18,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
  }
).addTo(map);

$(document).ready(function(){

  $(".nav-item .nav-link").click(function(){
    $(".nav-item .nav-link").parent().removeClass("active");
    $(this).parent().addClass("active");
  });
});

// map on contact-us
if($("#mapcontact").length){
  var mapContact = L.map("mapcontact").setView([51.505, -0.09], 12);
  var tiles = L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
    {
      maxZoom: 18,
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: "mapbox/streets-v11",
      tileSize: 512,
      zoomOffset: -1,
    }
  ).addTo(mapContact);
}

if($("#main-slider").length){
document.addEventListener( 'DOMContentLoaded', function () {
  var main = new Splide( '#main-slider', {
      type      : 'fade',
      rewind    : true,
      pagination: false,
      arrows    : false,
  } );
  var thumbnails = new Splide( '#thumbnail-slider', {
  fixedWidth:120,
  padding:30,
  gap: "2rem",
  rewind: true,
  arrows    : true,
  pagination: false,
      fixedHeight: 100,
      cover: true,
      isNavigation: true,
      breakpoints : {
          600: {
              fixedWidth : 70,
              fixedHeight: 60,
          },
      },
  } );
  main.sync( thumbnails );
  main.mount();
  thumbnails.mount();
} );
}