window.addEventListener("scroll", function () {
    var header = document.querySelector("#navTest");
    var logo = document.querySelector("#navTest .test");
    header.classList.toggle("sticky-test", window.scrollY > 300);
    logo.classList.toggle("hiden-logo", window.scrollY > 300);
  });
  
  var map = L.map("map").setView([51.505, -0.09], 12);
  var tiles = L.tileLayer(
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw",
    {
      maxZoom: 18,
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: "mapbox/streets-v11",
      tileSize: 512,
      zoomOffset: -1,
    }
  ).addTo(map);
  