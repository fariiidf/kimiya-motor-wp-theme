<?php

function kimiyaco_setup() {
    add_theme_support( 'post-thumbnails' );

    add_theme_support('custom-logo');
}
add_action( 'after_setup_theme', 'kimiyaco_setup' );

function register_my_menus() {
    register_nav_menus(
        array(
            'main-menu' => __( ' Main Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );


class SH_Nav_Menu_Walker extends Walker {
    var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
    var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

    function start_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent";
        $output .= "<i class=\"dropdown icon\"></i>\n";
        $output .= "<div class=\"menu\">\n";
    }

    function end_lvl(&$output, $depth) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</div>\n";
    }

    function start_el(&$output, $item, $depth, $args) {
        $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        if( $item->current == 1 ) {
            $classes[] = 'active';
        }
        $classes = in_array( 'active', $classes ) ? array( 'active nav-item' ) : array("nav-item");
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = strlen( trim( $class_names ) ) > 0 ? ' class="' . esc_attr( $class_names ) . '"' : '';
        $id = apply_filters( 'nav_menu_item_id', '', $item, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $item_output = $args->before;
        $item_output .= '<li'. $id . $value . $class_names . '>';
        $item_output .= '<a class="nav-link" '.$attributes . '>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</a>';
        $item_output .= "</li>\n";
        $item_output .= $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

function custom_post_type() {
 
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'محصولات', 'Post Type General Name', 'kimiyaco' ),
            'singular_name'       => _x( 'محصول', 'Post Type Singular Name', 'kimiyaco' ),
            'menu_name'           => __( 'محصولات', 'kimiyaco' ),
            'parent_item_colon'   => __( 'محصول مادر', 'kimiyaco' ),
            'all_items'           => __( 'همه محصولات', 'kimiyaco' ),
            'view_item'           => __( 'مشاهده محصول', 'kimiyaco' ),
            'add_new_item'        => __( 'افزودن محصول', 'kimiyaco' ),
            'add_new'             => __( 'افزودن جدید', 'kimiyaco' ),
            'edit_item'           => __( 'ویرایش محصول', 'kimiyaco' ),
            'update_item'         => __( 'بروزرسانی محصول', 'kimiyaco' ),
            'search_items'        => __( 'جستجوی محصولات', 'kimiyaco' ),
            'not_found'           => __( 'پیدا نشد', 'kimiyaco' ),
            'not_found_in_trash'  => __( 'در زباله دان پیدا نشد', 'kimiyaco' ),
        );
         
    // Set other options for Custom Post Type
         
        $args = array(
            'label'               => __( 'محصولات', 'kimiyaco' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies'          => array( 'genres' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */ 
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
     
        );
         
        // Registering your Custom Post Type
        register_post_type( 'products', $args );


 
    // Set UI labels for Custom Post Type
    $labels2 = array(
        'name'                => _x( 'پرسنل', 'Post Type General Name', 'kimiyaco' ),
        'singular_name'       => _x( 'پرسنل', 'Post Type Singular Name', 'kimiyaco' ),
        'menu_name'           => __( 'پرسنل', 'kimiyaco' ),
        'all_items'           => __( 'همه پرسنل', 'kimiyaco' ),
        'view_item'           => __( 'مشاهده', 'kimiyaco' ),
        'add_new_item'        => __( 'افزودن', 'kimiyaco' ),
        'add_new'             => __( 'افزودن جدید', 'kimiyaco' ),
        'edit_item'           => __( 'ویرایش', 'kimiyaco' ),
        'update_item'         => __( 'بروزرسانی', 'kimiyaco' ),
        'search_items'        => __( 'جستجوی پرسنل', 'kimiyaco' ),
        'not_found'           => __( 'پیدا نشد', 'kimiyaco' ),
        'not_found_in_trash'  => __( 'در زباله دان پیدا نشد', 'kimiyaco' ),
    );
     
// Set other options for Custom Post Type
     
    $args2 = array(
        'label'               => __( 'پرسنل', 'kimiyaco' ),
        'labels'              => $labels2,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
        'taxonomies'          => array( 'genres' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
 
    );
     
    // Registering your Custom Post Type
    register_post_type( 'personnel', $args2 );
     
}
     
    /* Hook into the 'init' action so that the function
    * Containing our post type registration is not 
    * unnecessarily executed. 
    */
     
    add_action( 'init', 'custom_post_type', 0 );


function create_projects_taxonomies() {
	$labels = array(
		'name'              => _x( 'دسته‌های محصولات', 'taxonomy general name' ),
		'singular_name'     => _x( 'دسته محصول', 'taxonomy singular name' ),
		'search_items'      => __( 'جستجوی دسته‌های محصولات' ),
		'all_items'         => __( 'همه دسته‌های محصولات' ),
		'parent_item'       => __( 'دسته مادر' ),
		'parent_item_colon' => __( 'دسته مادر:' ),
		'edit_item'         => __( 'ویرایش دسته' ),
		'update_item'       => __( 'به‌روز رسانی دسته' ),
		'add_new_item'      => __( 'افزودن دسته تازه' ),
		'new_item_name'     => __( 'نام دسته جدید' ),
		'menu_name'         => __( 'دسته‌های محصولات' ),
	);

	$args = array(
		'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'product-categories' ),
	);

	register_taxonomy( 'products_categories', array( 'products' ), $args );
}
add_action( 'init', 'create_projects_taxonomies', 0 );

if (version_compare($GLOBALS['wp_version'], '5.0-beta', '>')) {
	
	// WP > 5 beta
	add_filter('use_block_editor_for_post_type', '__return_false', 100);
	
} else {
	
	// WP < 5 beta
	add_filter('gutenberg_can_edit_post_type', '__return_false');
	
}

function template_chooser($template){
  global $wp_query;   
  $post_type = get_query_var('post_type');   
  if( $wp_query->is_search && $post_type == 'products' )   
  {
    return locate_template('archive-products.php');  //  redirect to archive-search.php
  }   
  return $template;   
}
add_filter('template_include', 'template_chooser');

function add_query_vars($aVars) {
    $aVars[] = "products"; 
    return $aVars;
}
add_filter('query_vars', 'add_query_vars');

// function add_rewrite_rules($aRules) {
//     $aNewRules = array('products/([^/]+)/?$' => 'index.php?pagename=products&s=$matches[1]');
//     $aRules = $aNewRules + $aRules;
//     return $aRules;
// }
    
// add_filter('rewrite_rules_array', 'add_rewrite_rules');

add_filter( 'excerpt_length', 'wpse_default_excerpt_length', 100 );
function wpse_default_excerpt_length( $length ) {
    return 20;
}

function page_redirect() {
    if (strpos($_SERVER['REQUEST_URI'], "/products/?paged") !== false)  {
        require(TEMPLATEPATH . '/archive-products.php');
    }else if(strpos($_SERVER['REQUEST_URI'], "/blog/?paged") !== false){
        require(TEMPLATEPATH . '/home.php');
    }
}
add_action('template_redirect', 'page_redirect');

function my_special_nav_class( $classes, $item ) {
    if ($item->title == 'محصولات' && strpos($_SERVER['REQUEST_URI'], "/products") !== false && !is_single()) {
        $classes[] = 'active';
    }
    return $classes;
}    
add_filter( 'nav_menu_css_class', 'my_special_nav_class', 10, 2 );

function getNextPageAddress($pageCount){
    if(isset($_GET["paged"])){
        if($pageCount <= $_GET["paged"]){
            return "#";
        }else {
            return add_query_arg(array('paged' => intval($_GET["paged"])+1));
        }
    }else{
        if($pageCount>1){
            return add_query_arg(array('paged' => 2));
        }else{
            return "#";
        }
    }
}
function getPrevPageAddress($pageCount){
    if(isset($_GET["paged"])){
        if($_GET["paged"] <= 1){
            return "#";
        }else {
            return add_query_arg(array('paged' => intval($_GET["paged"])-1));
        }
    }else{
        return "#";
    }
}

function isActive($page){
    if(isset($_GET['paged'])){
        if($page == $_GET['paged']){
            return true;
        }
    }else{
        if($page==1){
            return true;
        }else{
            return false;
        }
    }
}

function convertNums($string) {
    $persian = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    $arabic = ['٩', '٨', '٧', '٦', '٥', '٤', '٣', '٢', '١','٠'];

    $num = range(0, 9);
    $convertedPersianNums = str_replace($persian, $num, $string);
    $englishNumbersOnly = str_replace($arabic, $num, $convertedPersianNums);

    return $englishNumbersOnly;
}

function getLangUrl($lang){
    $all_langs = array("fa", "en");
    $current = array_values(array_filter(explode("/", $_SERVER["REQUEST_URI"])));
    if($lang == "fa"){
        if(in_array($current[0], $all_langs)){
            array_shift($current);
        }
    }else{
        if(in_array($current[0], $all_langs)){
            $current[0] = $lang;
        }else{
            array_unshift($current, $lang);
        }
    }
    return site_url(implode("/", $current));
}