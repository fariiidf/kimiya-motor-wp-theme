<?php
get_header();
?>
<div class="container mt-md-5">
    <div class="row">
        <div class="col-12 px-4 px-md-0">
            <ul class="nav d-flex flex-column flex-md-row nav-tabs border-0 " id="myTab" role="tablist">


                <?php
                $terms = get_categories(array("hide_empty"=>false));
                //var_dump($terms);
                foreach ($terms as $key => $value) {?>

                <li class="nav-item tab-container <?=$key==0 ? 'active' : ''?>" role="presentation">
                    <a href="<?=site_url($value->taxonomy."_".$value->slug)?>" class="nav-link <?=$value->slug?>"
                        id="home-tab" data-bs-toggle="tab" data-bs-target="#<?=$value->slug?>" type="button" role="tab"
                        aria-controls="<?=$value->slug?>" aria-selected="true"><span
                            class="text-<?=$value->slug?>"><?=$value->name?></span></a>

                </li>
                <style>
                    .nav-tabs <?='.'.$value->slug?>::before {
                        background: linear-gradient(rgb(0 0 0 / 0%), rgb(0 0 0 / 51%)), url("<?=get_field('image', $value->taxonomy."_".$value->term_id)["url"]?>") center;
                        background-size: cover;
                    }
                </style>

                <?php
                }
                ?>

            </ul>


            <div class="tab-content" id="blogPostsTab">


                <?php
                foreach ($terms as $key => $value) {?>

                <div class="tab-pane fade <?=$key==0 ? 'show active' : ''?>" id="<?=$value->slug?>" role="tabpanel"
                    aria-labelledby="<?=$value->slug?>-tab">
                    <div class="row mb-4">
                        <div class="col-12 text-center">
                            <h3 class="section-title fs-2 "><?=$value->name?></h3>
                        </div>
                    </div>
                    <?php
                    $perPage = 1;
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => $perPage,
                        'tax_query' => array(
                            array (
                                'taxonomy' => 'category',
                                'field' => 'term_id',
                                'terms' => $value->term_id,
                            )
                        )
                    );
                    
                    $my_query = new WP_Query( $args );
                    $total = $my_query->found_posts;

                    while($my_query->have_posts()) : $my_query->the_post();?>
                    <div class="row gx-0 gy-0 align-items-center mb-5">
                        <div class="col-md-4 py-md-4">
                            <a href="<?=get_the_permalink()?>">
                                <img src="<?=get_the_post_thumbnail_url()?>" class="img-fluid " alt="blog" /> </a>
                        </div>
                        <div class="col-md-4 shadow p-3 p-md-4 itemBox">
                            <a href="<?=get_the_permalink()?>" class="blogTitle">
                                <h3 class="h2 fw-bold"><?=get_the_title()?></h3>
                            </a>
                            <div class="card-seprator"></div>
                            <p class="text-muted my-4"><?=get_the_excerpt()?></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex justify-content-start">
                                    <a href="<?=get_the_permalink()?>" class="primary-button text-center">مطالعه مطلب<i
                                            class="bi bi-chevron-<?=get_locale()=="fa_IR" ? "left" : "right"?>"></i></a>
                                </div>
                                <div class=" d-flex justify-content-center align-items-center">
                                    <span><i
                                            class="bi bi-eye fs-3 text-muted d-inline-block border-start border-2 border-secondary lh-sm ps-2"></i></span>
                                    <span
                                        class="fw-bold text-black-50 pe-2"><?=get_field("visit_count", get_the_ID())==null ? "20" : get_field("visit_count", get_the_ID())?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endwhile; ?>
                    <!-- pagination -->
                    <?php
                    if($total > 0) {?>

                    <?php }else{ ?>
                    <p class="text-center">
                        محتوایی یافت نشد
                    </p>
                    <?php } ?>
                </div>


                <?php
                }
                ?>

            </div>

        </div>
    </div>
</div>
<?php
get_footer();
?>