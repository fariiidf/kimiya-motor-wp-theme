<?php
get_header();
?>

<main class="container pt-md-5">
    <div class="row gy-5 productsFiltersRow">
        <div class="col-md-3 productsFilters">
            <div class="input-group mb-3 shadow ">
                <form role="search" action="<?php echo site_url('/products/'); ?>" method="get" id="searchform">
                    <input type="text" class="form-control border-0 p-2"
                        value="<?=isset($_GET['s']) ? $_GET['s'] : ''?>" placeholder="Search" name="s">
                    <input type="hidden" name="post_type" value="products" />
                    <button class=" search-btn" type="submit" id="button-addon2"><i
                            class="bi bi-search text-white fs-5 ms-4"></i></button>
                </form>

            </div>
            <h3 class="side-title mt-5 text-black-50 pb-2 fw-bold border-bottom border-2 border-secondary">فیلتر جستجو</h3>

            <form method="post">
                <?php
            $args = array('taxonomy' => 'products_categories', 'hide_empty' => false);
            $mainCats = [];
            $terms = get_terms($args);
            
            foreach ($terms as $key => $value) {
                if($value->parent == 0){
                    $mainCats[$value->term_id]["name"] = $value->name;
                    $mainCats[$value->term_id]["subCats"] = [];
                }
            }
            foreach ($terms as $key => $value) {
                if($value->parent != 0){
                    $mainCats[$value->parent]["subCats"][$value->term_id] = $value->name;
                }
            }

            foreach ($mainCats as $key => $value) {
                echo '<p class="side-subtitle mt-3 text-black-50 lead fw-bold">'.$value["name"].'</p>';
                if($value["subCats"])
                    foreach ($value["subCats"] as $key2 => $value2) {?>
                <div class="form-check">
                    <input class="form-check-input" value="<?=$key2?>" type="checkbox" name="cats[]" id="term<?=$key2?>"
                        value="<?=$key2?>"
                        <?=(isset($_POST['cats']) && array_search($key2, $_POST['cats']) !== false) ? "checked" : ""?> />
                    <label class="side-items form-check-label text-muted" for="term<?=$key2?>">
                        <?=$value2?>
                    </label>
                </div>
                <?php
                    }
            }
            ?>
                <button name="filters" value="true" type="submit" class="primary-button w-100 p-5 mt-3">اعمال
                    فیلتر</button>
            </form>
        </div>
        <div class="col-md-8 offset-md-1">
            <div class="row gy-5 ">
                <?php
                $perPage = 10;
                $args = array(
                    'post_type' => 'products',
                    'posts_per_page' => $perPage,
                    'paged' => isset($_GET["paged"]) ? $_GET["paged"] : 0
                );
                if(isset($_POST['filters'])){
                    
                    $args["tax_query"] = array(
                        array(
                        "taxonomy" => "products_categories",
                        "field"    => 'term_id',
                        "terms"    =>  $_POST['cats'],
                        'operator' => 'IN'
                        )
                    );
                }else if(isset($_GET['s'])){
                    $args["s"] = convertNums($_GET['s']);
                    // wp_reset_query();
                    // wp_reset_postdata();
                }
                    
                $my_query = new WP_Query( $args );
                $total = $my_query->found_posts;
                while($my_query->have_posts()) : $my_query->the_post();?>
                <div class="col-md-6 ">
                    <div class="card card-product border-0 shadow">
                        <div class="d-flex justify-content-center">
                            <a href="<?=get_the_permalink()?>"><img src="<?=get_the_post_thumbnail_url()?>"
                                    class="card-img-top"
                                    alt="<?=get_post_meta(get_post_thumbnail_id(), '_wp_attachment_image_alt', TRUE)?>" />
                            </a>
                        </div>
                        <div class="d-flex justify-content-center">
                            <div class="card-seprator text-center"></div>
                        </div>
                        <div class="card-body text-center">
                            <a href="<?=get_the_permalink()?>">
                                <h5 class="card-title d-inline-block mb-4"><?=get_the_title()?></h5>
                            </a>
                            <div class="d-flex justify-content-center">
                                <a href="<?=get_the_permalink()?>" class="primary-button text-center"><i
                                        class="bi bi-chevron-left"></i>مشاهده محصول</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                endwhile;
                ?>
            </div>
        </div>
        <?php
                    if($total > 0) {?>
        <div class="container my-5">
            <div class="row">
                <div class="col-12 ">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item ">
                                <a class="page-link" href="<?=getPrevPageAddress($total/$perPage)?>">
                                    <i class="fa fa-chevron-left"></i>
                                </a>
                            </li>
                            <?php
                    for($k=0; $k<($total/$perPage); $k++){
                        $params = array('paged' => $k+1);
                        echo '
                        <li class="page-item '.(isActive($k+1) ? "active" : "").'"><a class="page-link" href="'.(isActive($k+1) ? "#" : add_query_arg($params)).'">'.($k+1).'</a></li>
                        ';
                    }
                    ?>
                            <li class="page-item">
                                <a class="page-link" href="<?=getNextPageAddress($total/$perPage)?>">
                                    <i class="fa fa-chevron-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <?php }else{ ?>
        <p class="text-center">
            محتوایی یافت نشد
        </p>
        <?php } ?>
    </div>
</main>

<?php 
get_footer();
?>