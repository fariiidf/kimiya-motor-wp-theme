<?php /* Template Name: درباره ما */ ?>
<?php get_header(); ?>
<!-- about-us -->
<div class="about_page">
  <div class="container container-lg-fluid about_page_top">
    <div class=" learn-more-container position-relative">
      <div class="col-lg-5">
        <div class="learn-more">
          <h1 class="mb-0 fw-bold text-primary titleH1"><?=get_field("about_title", 80)?></h1>
          <p class="lead fw-bold"><?=get_field("about_subtitle", 80)?></p>
          <span class="span"></span>
          <p class="text-muted">
            <?=get_field("about_text", 80)?>
          </p>
        </div>
      </div>
      <div class="col-lg-7 learn-moreabout-us-pic d-none d-lg-block ">
        <img src="<?=get_field("about_image", 80)?>" />
      </div>
    </div>
  </div>
  <div class="container ">
    <div class="row field_title">
      <div class="col-12 text-center">
        <h3 class="section-title fs-3 mt-0 mt-lg-5"><?=get_field("activity_field_title", 80)?></h3>
      </div>
    </div>
    <div class="row justify-content-center">
      <div class="col-md-8 text-start text-md-center">
        <p class="text-muted my-3 text-center">
          <?=get_field("activity_field_text", 80)?>
        </p>
      </div>
    </div>
    <div class="row gx-0 gy-0 align-items-center mb-5 justify-content-center mt-5">
      <div class="col-lg-5 offset-lg-1 shadow p-3 p-lg-2 text-box-aboutus text-box-aboutus-1 fs-20">
        <h3 class="h1 mt-2 text-white aboutTitle"><?=get_field("activity_title_1", 80)?></h3>
        <div class="text-white text-justify text-muted"><?=get_field("activity_text_1", 80)?></div>
      </div>
      <div class="col-lg-6 "><img src="<?=get_field("activity_image_1", 80)?>"
          class="img-fluid translate-image-aboutus w-100" alt="blog"></div>
    </div>
  </div>

  <div class="container mt-5">
    <div class="row gx-0 gy-0 align-items-center mb-5 justify-content-center ">
      <div class="col-lg-6 offset-lg-1"><img src="<?=get_field("activity_image_2", 80)?>" class="img-fluid  w-100"
          alt="blog"></div>
      <div class="col-lg-5  shadow p-3 p-lg-2 text-box-aboutus2 translate-image-aboutus2 fs-20">
        <h3 class="h1 mt-2 text-white aboutTitle"><?=get_field("activity_title_2", 80)?></h3>
        <div class="text-white text-justify text-muted"><?=get_field("activity_text_2", 80)?></div>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row gx-0 gy-0 align-items-center mb-5 justify-content-center mt-5">
    <div class="col-lg-5 offset-lg-1 shadow p-3 p-lg-2 text-box-aboutus text-box-aboutus-1 fs-20">
      <h3 class="h1 mt-2 text-white aboutTitle"><?=get_field("activity_title_3", 80)?></h3>
      <div class="text-white text-justify text-muted"><?=get_field("activity_text_3", 80)?></div>
    </div>
    <div class="col-lg-6 "><img src="<?=get_field("activity_image_3", 80)?>"
        class="img-fluid translate-image-aboutus w-100" alt="blog"></div>
  </div>
</div>
</div>
<div class="container">
  <div class="row">
    <div class="col-12 text-center">
      <h3 class="section-title fs-5"><?=get_field("awards_title", 80)?></h3>
    </div>
  </div>
  <div id="splideAwards" class="splide ">
    <div class="splide__arrows">
      <button class="splide__arrow splide__arrow--prev bg-transparent">
        <span class="right-arrow" aria-hidden="true"></span>
      </button>
      <button class="splide__arrow splide__arrow--next bg-transparent">
        <span class="left-arrow" aria-hidden="true"></span>
      </button>
    </div>
    <div class="splide__track">
      <ul class="splide__list">
        <?php
        $awards = get_field("awards", 80);
        foreach ($awards as $key => $value) {
          if($value==false){
            unset($awards[$key]);
          }else{?>
        <li class="splide__slide d-flex justify-content-center padding">
          <div class="card card-partner border-0 shadow" style="width: 18rem">
            <div class="d-flex justify-content-center   ">
              <img src="<?=$value?>" class="card-img-top w-100 h-100 " alt="..." />
            </div>
          </div>
        </li>
        <?php
        }
        }?>
      </ul>
    </div>
  </div>
</div>
<?php
    get_footer();
    ?>