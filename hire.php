<?php /* Template Name: استخدام */ ?>
<?php get_header(); ?>
<div class="container  mt-lg-15">
    <div class="row g-0  position-relative ">
        <div class="col-lg-7 ">
            <div class="col-12 text-center shadow get-hiring-title p-5">
                <h3 class="section-title mt-0"><?=the_field("title")?></h3>
                <p class="text-muted my-3">
                    <?=the_field("text")?>
                </p>
            </div>
        </div>
        <div class="col-lg-4 hiringForm">
            <div class="form-hiring">
                <div class="badge-hiring"></div>
                <?=do_shortcode('[contact-form-7 id="225" title="فرم استخدام"]');?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>