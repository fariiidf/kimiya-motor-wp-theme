<?php
get_header();
?>


<div class="container_fluid greyBg singleProductTop">
    <div class="row align-items-center">
        <div class="col-md-4 offset-md-1 ">
            <div id="main-slider" class="w-100 h-100">
                <div class="splide__track">
                    <ul class="splide__list d-flex justify-content-center">
                        <?php
                        foreach (get_field("images") as $key => $value) {
                            if($value)
                                echo '
                                <li class="splide__slide d-flex justify-content-center">
                                    <img src="'.$value["url"].'">
                                </li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-6 product-discription">
            <h2 class="section-title px-0 mt-0"><?=the_title()?></h2>
            <p class="h6 text-muted my-3"><?=the_field("car")?></p>
            <p class="h6 text-muted mb-3"><?=get_the_terms(get_the_id(), "products_categories")[0]->name?></p>
            <!-- thumbnail-slider -->
            <div id="thumbnail-slider" class="">
                <div class="splide__arrows">
                    <button class="splide__arrow splide__arrow--prev bg-transparent">
                        <span class="right-arrow" aria-hidden="true"></span>
                    </button>
                    <button class="splide__arrow splide__arrow--next bg-transparent">
                        <span class="left-arrow" aria-hidden="true"></span>
                    </button>
                </div>
                <div class="splide__track">
                    <ul class="splide__list d-flex justify-content-center align-items-center">
                        <?php
                        foreach (get_field("images") as $key => $value) {
                            if($value)
                                echo '
                                <li class="splide__slide">
                                    <img src="'.$value["url"].'">
                                </li>';
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- discriptions -->
<div class="container singleProductContentContainer">
    <div class="row justify-content-center ">
        <div class="col-12 col-md-10 text-center shadow  p-md-5 singleProductContent">
            <h3 class="section-title fs-3 mt-2 mt-md-0  mb-4">تـوضـیحات</h3>
            <p class="text-muted my-3 lh-lg " style="text-align: justify;">
                <?=the_content()?>
            </p>
        </div>
    </div>
</div>
<!-- other product -->
<div class="container mb-5">
    <div class="row">
        <div class="col-12 text-center">
            <h3 class="section-title fs-5 mt-5">سایـر محصـولات</h3>
        </div>
    </div>
    <div id="splide-other-product" class="splide">
        <div class="splide__arrows">
            <button class="splide__arrow splide__arrow--prev bg-transparent">
                <span class="right-arrow" aria-hidden="true"></span>
            </button>
            <button class="splide__arrow splide__arrow--next bg-transparent">
                <span class="left-arrow" aria-hidden="true"></span>
            </button>
        </div>
        <div class="splide__track">
            <ul class="splide__list">
                <?php
                $args = array(
                    'post_type' => 'products',
                    'posts_per_page' => 8,
                    'orderby'        => 'rand',
                );
                    
                $my_query = new WP_Query( $args );
                while($my_query->have_posts()) : $my_query->the_post();?>
                <li class="splide__slide d-flex justify-content-center padding">
                    <div class="card card-product border-0 shadow" style="width: 18rem">
                        <div class="d-flex justify-content-center">
                            <img src="<?=get_the_post_thumbnail_url()?>" class="card-img-top" alt="..." />
                        </div>
                        <div class="d-flex justify-content-center">
                            <div class="card-seprator text-center"></div>
                        </div>
                        <div class="card-body itemBox">
                            <h5 class="card-title text-start"><?=the_title()?></h5>
                            <div class="card-text text-muted text-start">
                                <?=the_excerpt()?>
                            </div>
                            <div class="d-flex justify-content-center">
                                <a href="<?=get_the_permalink()?>" class="primary-button text-center">مشاهده محصول<i
                                        class="bi bi-chevron-<?=get_locale()=="fa_IR" ? "left" : "right"?>"></i></a>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
                endwhile;
                ?>


            </ul>
        </div>
    </div>
</div>

<?php
get_footer();
?>