<?php /* Template Name: نمایندگان */ ?>
<?php get_header(); ?>
<style>
    .agentsContainer{
        direction: rtl;
        text-align: right;
    }
    .highcharts-exporting-group{
        display: none;
    }
    .highcharts-label{
        transform: translate(0,10) !important;
    }
    #container {
        height: 600px;
        min-width: 310px;
        max-width: 100%;
        margin: 4rem auto 2rem;
    }

    .loading {
        margin-top: 10em;
        text-align: center;
        color: gray;
    }
</style>
<script src="https://code.highcharts.com/maps/highmaps.js"></script>
<script src="https://code.highcharts.com/maps/modules/exporting.js"></script>
<script src="<?= get_template_directory_uri() ?>/js/map.js"></script>

<div id="container"></div>
<div class="agentsContainer container">
    <div class="row align-items-top d-flex agentsBox"></div>
</div>

<script>
    (async () => {

const topology = await fetch(
    'https://code.highcharts.com/mapdata/countries/ir/ir-all.topo.json'
).then(response => response.json());

// Prepare demo data. The data is joined to map using value of 'hc-key'
// property by default. See API docs for 'joinBy' for more info on linking
// data and map.
const data = [
    ['ir-5428', 10],
    ['ir-hg', 11],
    ['ir-bs', 12],
    ['ir-kb', 13],
    ['ir-fa', 14],
    ['ir-es', 15],
    ['ir-sm', 16],
    ['ir-go', 17],
    ['ir-mn', 18],
    ['ir-th', 19],
    ['ir-mk', 20],
    ['ir-ya', 21],
    ['ir-cm', 22],
    ['ir-kz', 23],
    ['ir-lo', 24],
    ['ir-il', 25],
    ['ir-ar', 26],
    ['ir-qm', 27],
    ['ir-hd', 28],
    ['ir-za', 29],
    ['ir-qz', 30],
    ['ir-wa', 31],
    ['ir-ea', 32],
    ['ir-bk', 33],
    ['ir-gi', 34],
    ['ir-kd', 35],
    ['ir-kj', 36],
    ['ir-kv', 37],
    ['ir-ks', 38],
    ['ir-sb', 39],
    ['ir-ke', 40],
    ['ir-al', 41]
];

var agents = {
    "ir-th": [
        {
            "name": "فروشگاه مگا سازه",
            "address": "خیابان ملت، کوچه کاوه، پلاک ۸",
            "tell": "02133911952 - 02133966610"
        },
        {
            "name": "فروشگاه انصاری",
            "address": "خیابان اکباتان، کوچه آهنین، بن بست سوم، پاساژ فروغی، پلاک ۶۶",
            "tell": "02133918986 - 02133945618"
        },
        {
            "name": "بازرگانی پاسارگاد",
            "address": "خیابان ملت، کوچه کاوه، پلاک ۳۸",
            "tell": "2133914515"
        },
        {
            "name": "فروشگاه اژدری",
            "address": "خیابان اکباتان، کوچه آهنین، بن بست سوم، پلاک ۳",
            "tell": "02133951827 - 02133918167"
        },
        {
            "name": "فروشگاه غفاری",
            "address": "خیابان اکباتان، کوچه آهنین، بن بست سوم، پاساژ فروغی، پلاک ۳۸",
            "tell": "02133921279 - 02133911745"
        },
        {
            "name": "فروشگاه نقاش زاده",
            "address": "خیابان اکباتان، کوچه 8متری ملت، پلاک ۱",
            "tell": "02133929909 - 02133938066"
        },
        {
            "name": "پژو داریوش",
            "address": "ابتدای خیابان ملت، پلاک ۳۳",
            "tell": "02133972975 - 02133915116"
        },
        {
            "name": "بازرگانی رادیکال",
            "address": "عباس آباد خ بهشتی خ اندیشه، کوچه اندیشه 1 غربی، پلاک ۲۹",
            "tell": "2145340000"
        },
        {
            "name": "الکا موتور",
            "address": "خیابان ملت، کوچه قدیم نوایی، پلاک ۱۸",
            "tell": "02133937416 - 02133986943"
        },
        {
            "name": "فروشگاه امیری",
            "address": "خ پیروزی خ نبرد شمالی پایین تر از چهار راه ایمه اطهار، کوچه مونسان، پلاک ۲۲",
            "tell": "9125224092"
        }
    ],
    "ir-al": [
        {
            "name": "فروشگاه سورن",
            "address": "خ شهید بهشتی،  میانجاده، مجتمع البرز، بلوک C، واحد 7",
            "tell": "9125663917"
        },
        {
            "name": "فروشگاه باصری",
            "address": "بلوار چمران، پاساژرضا، پلاک 52",
            "tell": "9122681301"
        },
        {
            "name": "فروشگاه موثقی",
            "address": "بلوار چمران، پاساژرضا، پلاک 66",
            "tell": "9122607207"
        }
    ],
    "ir-kz": [
        {
            "name": "فروشگاه راتق",
            "address": "اهواز، اتوبان آیت اله بهبهانی، جنب نمایندگی ایران خودرو زیبایی، کوچه کارت طلایی (کیومرث)، پلاک 11",
            "tell": "6135518631"
        }
    ],
    "ir-bs": [
        {
            "name": "فروشگاه امینی",
            "address": " بلوار طالقانی خیابان صدرا فرعی اول سمت راست جنب کوکا کولا",
            "tell": "9171745021"
        }
    ],
    "ir-kv": [
        {
            "name": "فروشگاه حلوایی",
            "address": "مشهد، بلوار جمهوری، جنب خ جمهوری، پلاک 12",
            "tell": "9151118851"
        }
    ],
    "ir-lo": [
        {
            "name": "فروشگاه یار احمدی",
            "address": "دورود، کوی جهاد گران نبش اخلاص، پلاک 16",
            "tell": "9106051216"
        }
    ],
    "ir-wa": [
        {
            "name": "فروشگاه کامیار",
            "address": "مهاباد",
            "tell": "4442334030"
        }
    ],
    "ir-kd": [
        {
            "name": "فروشگاه احمدی نیا",
            "address": "سنندج، شهرک صنعتی تعمیر کاران خ اول، فروشگاه آبیدر یدک",
            "tell": "9181710017"
        }
    ],
}

// Create the chart
Highcharts.mapChart('container', {
    chart: {
        map: IranMap,
    },
    title: {
        text: 'جهت مشاهده نمایندگی‌های هر شهر روی استان مورد نظر کلیک نمایید'
    },

    subtitle: {
        text: ''
    },
    tooltip: {
        headerFormat: '',
        pointFormat: '<span style="font-size:15px">{point.name}</span>',
    },
    legend: {
        enabled: false,
    },
    
    exportingGroup: {
        enabled: false,
    },
    
    credits: {
        enabled: false,
    },

    plotOptions: {
        series: {
            events: {
                click: function (e) {
                    var text = '<h3 class="title">استان: <b>'+e.point.name+'</b></h3>'+'<h4 class="subtitle">لیست نمایندگان:</h4>';
                    var currAgents = agents[e.point["hc-key"]];
                    for(i=0; i<currAgents.length; i++){
                        text += '<div class="col-md-4 col-sm-6 mb-4">';
                        text += '<div class="card h-100">';
                        text += '<div class="card-body">';
                        text += '<h6 class="card-title"><b>' + currAgents[i].name + '</b></h6>';
                        text += '<p class="card-text">آدرس: ' + currAgents[i].address + '</p>';
                        text += '<a class="card-text">شماره تماس: ' + currAgents[i].tell + '</a>';
                        text += '</div>';
                        text += '</div>';
                        text += '</div>';
                    }
                    if (!this.chart.clickLabel) {
                            $(".agentsBox").html(text);
                            $('html, body').animate({
                                scrollTop: $(".agentsBox").offset().top + -175
                            }, 1000);
                    } else {
                        this.chart.clickLabel.attr({
                            text: text
                        });
                    }
                }
            }
        }
    },
    mapNavigation: {
        enabled: false,
        buttonOptions: {
            verticalAlign: 'top',
            align: 'left',
        }
    },
    
    colorAxis: {
        min: 1,
        max: 25,
        type: 'logarithmic',
        minColor: '#ff0009',
        maxColor: '#590e10'
    },

    series: [{
        data: data,
        name: '',
        states: {
            hover: {
                color: '#39191a'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});

})();

</script>

<?php get_footer(); ?>