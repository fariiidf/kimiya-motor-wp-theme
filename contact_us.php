<?php /* Template Name: تماس با ما */ ?>
<?php get_header(); ?>

<!-- contact-us header -->
<div class="contact-us-header ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3 mt-md-5 d-flex justify-content-center"><img
                    src="<?=get_template_directory_uri().'/assets/images/logo-type.png'?>" class="img-fluid"
                    alt="kimiya-logo"></div>
        </div>
        <div class="row mt-5">
            <div class="col-12 text-center">
                <p class="text-muted"><?=the_field("text")?></p>
            </div>
        </div>
        <div class="row justify-content-center mt-3 contact-titleContainer">
            <h4 class="text-center  col-md-5 contact-title h6"><span><?=the_field("box_text")?></span></h4>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 social-contact-icons ps-3 text-center">
                <div class="social-badge"></div>
                <a href="<?=the_field("fb_url")?>"><i class="fa fa-instagram"></i></a>
                <a href="<?=the_field("insta_url")?>"><i class="fa fa-whatsapp"></i></a>
                <a class="facebook" href="<?=the_field("wa_url")?>"><i class="fa fa-facebook"></i></a>
                <div class="social-badge"></div>
            </div>
        </div>

    </div>
</div>
<!-- map and form -->
<div class="container my-10 contact-sec">
    <div class="row gy-4 position-relative mapAndForm">
        <div class="col-12 col-lg-4 contact-us-form ">
            <div class="row">
                <div class="col-12 ">
                    <h3 class="text-white h5">تماس با ما</h3>
                    <div class="d-flex footer-icon-group">
                        <i class="bi bi-geo-alt-fill"></i>
                        <p class="small text-white">
                            <?=the_field("address")?>
                        </p>
                    </div>
                    <div class="d-flex footer-icon-group">
                        <i class="bi bi-telephone-fill"></i>
                        <p class="small text-white"><?=the_field("tell")?></p>
                    </div>
                    <?=do_shortcode('[contact-form-7 id="211" title="فرم ارسال پیام فوری"]');?>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div id="mapcontact" style="height: 300px;"></div>
        </div>
    </div>
</div>


<?php get_footer(); ?>