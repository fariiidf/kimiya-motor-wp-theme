<footer class="position-relative mt-md-5 mt-lg-15">
  <div class="bg-footer">
    <div class="container">
      <div class="row gx-0">
        <div class="col-lg-1">
          <div class="footer-logo">
            <div class="ms-lg-4 ps-lg-4 mt-lg-3 mb-lg-2">
                <img id="footerLogo" alt="kimiya-logo" src="<?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                echo $image[0];
              ?>"/>
            </div>
            <h5 class="ms-lg-4 ps-lg-4 mt-lg-2 text-white"><?=get_field("footer_logo_sub", 5)?></h5>
            <span class="footerSpan"></span>
            <p class="ms-lg-4 ps-lg-4 mt-lg-4 pb-lg-5 aboutTxt">
              <?=get_field("footer_about", 5)?>
            </p>
          </div>
        </div>
        <div class="col-lg-6">
          <h5 class="text-white text-center text-lg-end footerContactTitle">اطـلاعات تمـاس</h5>
          <div class="map-footer">
            <div id="map"></div>
          </div>
        </div>
        <div class="col-lg-3 d-flex flex-column justify-content-center py-3">
          <div class="d-flex footer-icon-group">
            <i class="bi bi-geo-alt-fill"></i>
            <p class="small text-white">
              <?=get_field("footer_address", 5)?>
            </p>
          </div>
          <div class="d-flex footer-icon-group">
            <i class="bi bi-telephone-fill"></i>
            <p class="small text-white"><a
                href="tel:<?=get_field("footer_tell", 5)?>"><?=get_field("footer_tell", 5)?></a></p>
          </div>
          <p class="smal text-white ps-3">follow us:</p>
          <div class="social-icons ps-3">
            <a href="<?=get_field("footer_instagram", 5)?>"><i class="fa fa-instagram"></i></a>
            <a href="<?=get_field("footer_whatsapp", 5)?>"><i class="fa fa-whatsapp"></i></a>
            <a class="facebook" href="<?=get_field("footer_fb", 5)?>"><i class="fa fa-facebook"></i></a>
          </div>
        </div>
        <div class="col-lg-3 text-center">
          <!-- <h5 class="text-white mb-3">LINK</h5>
              <ul class="ps-0">
                <li class="list-footer"><a href="#">Lorem ipsum dolor</a></li>
                <li class="list-footer"><a href="#">Lorem ipsum dolor</a></li>
                <li class="list-footer"><a href="#">Lorem ipsum dolor</a></li>
                <li class="list-footer"><a href="#">Lorem ipsum dolor</a></li>
                <li class="list-footer"><a href="#">Lorem ipsum dolor</a></li>
              </ul> -->
        </div>
      </div>
    </div>
  </div>
  <div class="copy-right text-center"><a href="https://bytegroup.ir" target="_blank">طراحی وبسایت و سئو توسط
      <span>بایـت</span></a>
  </div>
</footer>
<?php
  if(get_locale() == "fa_IR"){
    echo '<script>var currLocale = "fa";</script>';
    echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/fa_fonts.css" />';
  }else if(get_locale() == "en_US"){
    echo '<script>var currLocale = "en";</script>';
    echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/css/en_fonts.css" />';
  }
  ?>

<script src="<?=get_template_directory_uri()?>/js/bootstrap.bundle.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/splide.min.js"></script>

<script src="<?=get_template_directory_uri()?>/js/jquery-3.6.0.min.js"></script>
<script src="<?=get_template_directory_uri()?>/js/home.js"></script>
<script src="<?=get_template_directory_uri()?>/js/wow.min.js"></script>

<script>
  new WOW().init();
  var elms1 = document.getElementsByClassName("indexProductsSplide");
  for (var i = 0; i < elms1.length; i++) {
    new Splide(elms1[i], {
      classes: {
        page: "splide__pagination__page your-class-page",
      },
      perPage: 3,
      gap: "2rem",
      height: "100%",
      direction: (currLocale == "fa") ? "rtl" : "ltr",
      arrows: true,
      padding: "2rem",
      breakpoints: {
        992: {
          padding: "0rem",
          gap: "2.5rem",
          perPage: 2,

        },
        640: {
          perPage: 1,
          gap: "0rem",
          height: "100%",
          padding: "0rem",

        },
        480: {
          perPage: 1,
          gap: "0rem",
          height: "100%",
          padding: "0rem",
        },
      },
    }).mount();
  }

  var elms = document.getElementsByClassName("splide");
  for (var i = 0; i < elms.length; i++) {
    new Splide(elms[i], {
      classes: {
        page: "splide__pagination__page your-class-page",
      },
      perPage: elms[i].id == "splideAwards" ? 4 : 3,
      gap: "6rem",
      height: "100%",
      direction: currLocale == "fa" ? 'rtl' : 'ltr',
      padding: "4rem",
      breakpoints: {
        992: {
          padding: "0rem",
          gap: "2.5rem",
          perPage: 2,

        },
        640: {
          perPage: 1,
          gap: "0rem",
          height: "100%",
          padding: "0rem",

        },
        480: {
          perPage: 1,
          gap: "0rem",
          height: "100%",
          padding: "0rem",
        },
      },
    }).mount();
  }
</script>

<?php wp_footer();?>
</body>

</html>