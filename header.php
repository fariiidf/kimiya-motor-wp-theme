<!DOCTYPE html>
<html lang="<?=get_locale()?>">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <script src="<?=get_template_directory_uri()?>/js/leaflet.js"></script>

  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/leaflet.css">
  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/bootstrap-icons.css">
  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/splide.min.css" />
  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/css/animate.min.css" />
  <link rel="stylesheet" href="<?=get_template_directory_uri()?>/style.css" />

  <!-- FotnAwesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  <title><?=bloginfo('name')?></title>
  <meta name="description" content="<?=bloginfo('description')?>">
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="<?=site_url()?>" />
  <link rel="icon" type="image/x-icon" href="<?=get_template_directory_uri()?>/favicon.png">
  <link rel="apple-touch-icon" href="<?=get_template_directory_uri()?>/favicon.png" />
  <meta property="og:site_name" content="<?=bloginfo('name')?>">
  <meta property="og:title" content="<?=bloginfo('name')?>" />
  <meta property="og:description" content="<?=bloginfo('description')?>" />
  <meta property="og:image" itemprop="image" content="<?=get_template_directory_uri()?>/assets/images/logo.png">

  <?php wp_head();?>

</head>

<body>
  <header class="d-flex flex-column-reverse flex-md-column">
    <div id="carouselHeader" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-inner position-relative">
        <div class="highlight-heder d-none d-md-block"></div>
        <?php
        $slide1 = get_field("header_slide_1", 5);
        $slide2 = get_field("header_slide_2", 5);
        $slide3 = get_field("header_slide_3", 5);
        ?>
        <?php if($slide1["header_img_1"]){?>
        <div class="carousel-item active">
          <img src="<?=$slide1["header_img_1"]?>" class="d-block w-100" alt="..." />
          <div class="carousel-caption ">
            <h5 class="carousel-tittle"><?=$slide1["header_title_1"]?></h5>
            <p><?=$slide1["header_subtitle_1"]?></p>
          </div>
        </div>
        <?php } ?>
        <?php if($slide2["header_img_2"]){?>
        <div class="carousel-item <?=($slide1==null) ? "active" : ""?>">
          <img src="<?=$slide2["header_img_2"]?>" class="d-block w-100" alt="..." />
          <div class="carousel-caption ">
            <h5 class="carousel-tittle"><?=$slide2["header_title_2"]?></h5>
            <p><?=$slide2["header_title_2"]?></p>
          </div>
        </div>
        <?php } ?>
        <?php if($slide3["header_img_3"]){?>
        <div class="carousel-item <?=($slide1==null && $slide2==null) ? "active" : ""?>">
          <img src="<?=$slide3["header_img_3"]?>" class="d-block w-100" alt="..." />
          <div class="carousel-caption ">
            <h5 class="carousel-tittle"><?=$slide3["header_title_3"]?></h5>
            <p><?=$slide3["header_title_3"]?></p>
          </div>
        </div>
        <?php } ?>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselHeader" data-bs-slide="prev">
        <span class="right-arrow"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselHeader" data-bs-slide="next">
        <span class="left-arrow"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
    <div class="container ">
      <nav id="navTest" class="navbar navbar-expand-lg navbar-light bg-white navbar-device">
        <div class="container">
          <a class="d-none d-lg-block navbar-brand" href="#">
            <div class="sticky-test-logo">
              <img class="w-75 " alt="kimiya-logo" src="<?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                echo $image[0];
              ?>"/>
            </div>
          </a>
          <button class="navbar-toggler order-md-2 primaryBg" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="fs-1"><i class="bi bi-list"></i></span>
          </button>
          <div class="test">
            <div class="imgLogo">
              <img alt="kimiya-logo" src="<?php 
                $custom_logo_id = get_theme_mod( 'custom_logo' );
                $image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                echo $image[0];
              ?>"/>
            </div>
          </div>
          <div class="btn-lang-group order-md-1 order-lg-2">
            <a href="<?=getLangUrl("en")?>"><button
                class="btn-lang en <?=get_locale()=="en_US" ? "active" : ""?>"><span>EN</span></button></a>
            <a href="<?=getLangUrl("fa")?>"><button
                class="btn-lang pe <?=get_locale()=="fa_IR" ? "active" : ""?>"><span>FA</span></button></a>

          </div>
          <div class="collapse navbar-collapse order-lg-1" id="navbarText">
            <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'main-menu',
                  'menu_class' => 'navbar-nav ul-test me-auto mb-2 mb-lg-0',
                  'depth'         => 1,
                  'fallback_cb'   => false,
                  'nav_menu_css_class' => "nav-item",
                  'walker' => new SH_Nav_Menu_Walker
                )
              );
              ?>
            <!-- <ul class="navbar-nav ul-test me-auto mb-2 mb-lg-0">
                <li class="nav-item active">
                  <a class="nav-link" aria-current="page" href="#">Home</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Products</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Blog</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">About Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Contact Us</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="#">Hiring </a>
                </li>
              </ul> -->

          </div>
        </div>
      </nav>
    </div>
  </header>